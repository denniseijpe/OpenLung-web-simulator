import { connect } from 'react-redux'

import Pressure from '../components/Pressure'

const mapStateToProps = (state) => ({
  pressure: state.breathingSimulator.pressure,
})

export default connect(mapStateToProps)(Pressure)
