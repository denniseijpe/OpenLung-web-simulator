import { connect } from 'react-redux'

import BreathingMonitor from '../components/BreathingMonitor'
import { breathingSimulatorTick, FPS } from '../modules/breathing-simulator'

const mapStateToProps = () => ({
  fps: FPS,
})

const mapDispatchToProps = ({
  breathingSimulatorTick,
})

export default connect(mapStateToProps, mapDispatchToProps)(BreathingMonitor)
