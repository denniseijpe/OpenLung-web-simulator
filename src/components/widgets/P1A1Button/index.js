import React from 'react'

const P1A1Button = ({ color }) => (
  <div className="container">
    <div className={`button ${color}`} />
    <style jsx>
      {`
        .container {
          background: #000;
          display: inline-block;
          padding: 0.5em;
        }

        .button {
          width: 5em;
          height: 5em;
        }

        .blue {
          background: radial-gradient(circle, rgba(0,8,85,1) 0%, rgba(16,0,148,1) 100%); 
          border: 3px solid rgb(0, 8, 85);
        }

        .green {
          background: radial-gradient(circle, rgba(0,85,9,1) 0%, rgba(0,130,49,1) 100%);
          border: 3px solid rgb(0, 85, 9);
        }
      `}
    </style>
  </div>
)

export default P1A1Button
