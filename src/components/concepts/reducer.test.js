import controllerReducer, {
  setControllerSetting,
  SET_CONTROLLER_SETTING,
} from './reducer'

describe('Controller', () => {
  describe('Reducer', () => {
    it('should export constant SET_CONTROLLER_SETTING', () => {
      expect(SET_CONTROLLER_SETTING).toEqual('SET_CONTROLLER_SETTING')
    })

    it('should export action setControllerSetting', () => {
      expect(setControllerSetting({ test: 5 })).toEqual({
        type: SET_CONTROLLER_SETTING,
        payload: {
          test: 5,
        },
      })
    })

    it('should handle SET_CONTROLLER_SETTING', () => {
      let state = controllerReducer(undefined, {})

      // TODO: test default value

      // Dispatch RUN_SIMULATOR
      state = controllerReducer(state, {
        type: SET_CONTROLLER_SETTING,
        payload: {
          pressure: 10,
        },
      })

      // The pressure should be in the state
      expect(state.pressure).toEqual(10)
    })
  })
})
