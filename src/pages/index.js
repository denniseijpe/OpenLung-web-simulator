import React from 'react'
import Head from 'next/head'
import { Typography } from '@material-ui/core'

const Home = () => (
  <div>
    <Head>
      <title>OpenLung Simulator</title>
    </Head>
    <Typography>Please select a controller in the menu</Typography>
  </div>
)

export default Home
