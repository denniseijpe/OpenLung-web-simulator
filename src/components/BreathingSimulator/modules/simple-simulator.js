/**
 * Simple Simulator based on linearly reaching some values.
 */
class SimpleSimulator {
  constructor(fps) {
    this.pressure = 0
    this.pressureDiff = 1.8
    this.sleep = 0
    this.fps = fps
  }

  /**
   * Loop method of the simulation.
   */
  tick() {
    if (this.sleep > 0) {
      this.sleep -= 1
      return this.pressure
    }

    // Slow part of breath in
    if (this.pressure > 10 && this.pressureDiff > 0) {
      this.pressureDiff = 0.15
    }

    // Breath out
    if (this.pressure > 13 && this.pressureDiff > 0) {
      this.pressureDiff = -1.2
    }

    // Fast part of breath in
    if (this.pressure <= 0 && this.pressureDiff < 0) {
      this.pressureDiff = 1.8
    }

    this.pressure += this.pressureDiff

    // Pause for some time
    if (this.pressure < 0) {
      this.pressure = 0
      this.sleep = this.fps
    }

    return this.pressure
  }
}

export default SimpleSimulator
