import React from 'react'
import { shallow } from 'enzyme'

import Pressure from './Pressure'

describe('Vitals', () => {
  describe('[component] Pressure', () => {
    it('should render the pressure', () => {
      const wrap = shallow(<Pressure pressure={10} />)

      expect(wrap.text()).toContain('10')
    })
  })
})
