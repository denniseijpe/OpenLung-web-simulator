import React from 'react'
import MaterialUIDrawer from '@material-ui/core/Drawer'
import List from '@material-ui/core/List'
import ListItem from '@material-ui/core/ListItem'
import ListItemText from '@material-ui/core/ListItemText'
import Typography from '@material-ui/core/Typography'
import Link from 'next/link'

/**
 * Renders a menu for selecting the different concepts.
 */
const Drawer = () => (
  <MaterialUIDrawer
    variant="persistent"
    anchor="left"
    open
    PaperProps={{ style: { width: '15em' } }}
  >
    <List>
      <ListItem>
        <Typography variant="h5">Concepts</Typography>
      </ListItem>
      <ListItem button>
        <Link href="/demo">
          <ListItemText primary="Demo" />
        </Link>
      </ListItem>
      <ListItem button>
        <Link href="/P1A1">
          <ListItemText primary="P1A1" />
        </Link>
      </ListItem>
    </List>
  </MaterialUIDrawer>
)

export default Drawer
