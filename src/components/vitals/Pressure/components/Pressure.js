import React from 'react'
import PropTypes from 'prop-types'

/**
 * Renders the current pressure.
 */
const Pressure = ({ pressure }) => (
  <span>
    {Math.round(pressure * 10) / 10}
  </span>
)

Pressure.propTypes = {
  pressure: PropTypes.number,
}

Pressure.defaultProps = {
  pressure: 0,
}

export default Pressure
