import React from 'react'
import PropTypes from 'prop-types'
import { VictoryChart, VictoryTheme, VictoryLine } from 'victory'

const PressureGraph = ({ pressureList }) => (
  <VictoryChart
    theme={VictoryTheme.material}
    height={250}
    width={400}
  >
    <VictoryLine
      style={{
        data: { stroke: '#c43a31' },
        parent: { border: '1px solid #ccc' },
      }}
      data={pressureList.map((p, idx) => ({ x: idx / 30, y: p }))}
    />
  </VictoryChart>
)

PressureGraph.propTypes = {
  pressureList: PropTypes.arrayOf(PropTypes.number),
}

PressureGraph.defaultProps = {
  pressureList: [],
}

export default PressureGraph
