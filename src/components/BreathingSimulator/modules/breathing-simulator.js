import { createAction, handleActions } from 'redux-actions'

import SimpleSimulator from './simple-simulator'

export const FPS = 30
const simulator = new SimpleSimulator(FPS)

/**
 * Constants
 */
export const BREATHING_SIMULATOR_TICK = 'BREATHING_SIMULATOR_TICK'

/**
 * Actions
 */
export const breathingSimulatorTick = createAction(BREATHING_SIMULATOR_TICK)

/**
 * Action handlers
 */
const handleTick = (state) => {
  const result = simulator.tick()

  // Store last 100 pressures
  let pressureList = state.pressureList.slice()
  pressureList.push(result)
  pressureList = pressureList.slice(-100)

  return {
    ...state,
    pressure: result,
    pressureList,
  }
}

/**
 * Reducer
 */
export const initialState = {
  pressure: 0,
  pressureList: [],
}

export const reducer = handleActions({
  [BREATHING_SIMULATOR_TICK]: handleTick,
}, initialState)

export default reducer
