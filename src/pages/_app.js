/* eslint-disable react/jsx-props-no-spreading */
import React from 'react'
import PropTypes from 'prop-types'
import withRedux from 'next-redux-wrapper'
import { Provider } from 'react-redux'
import CssBaseline from '@material-ui/core/CssBaseline'
import { createMuiTheme, ThemeProvider } from '@material-ui/core/styles'
import green from '@material-ui/core/colors/green'
import red from '@material-ui/core/colors/red'
import Head from 'next/head'
import 'typeface-roboto/index.css'

import BreathingMonitor from '../components/BreathingSimulator'
import Drawer from '../components/Drawer'
import makeStore from '../store'

const theme = createMuiTheme({
  palette: {
    primary: green,
    secondary: red,
  },
})

/**
 * Custom Next.JS App component for attaching Redux.
 */
const App = ({ Component, pageProps, store }) => (
  <Provider store={store}>
    <Head>
      <link href="https://fonts.googleapis.com/css2?family=Share+Tech+Mono&display=swap" rel="stylesheet" />
    </Head>
    <CssBaseline>
      <ThemeProvider theme={theme}>
        <Drawer />
        <div style={{ marginLeft: '16em' }}>
          <Component {...pageProps} />
        </div>
        <BreathingMonitor />
      </ThemeProvider>
    </CssBaseline>
  </Provider>
)

App.propTypes = {
  Component: PropTypes.func,
  pageProps: PropTypes.shape({}),
  store: PropTypes.shape({}).isRequired,
}

App.defaultProps = {
  Component: () => <div />,
  pageProps: {},
}

export default withRedux(makeStore)(App)
