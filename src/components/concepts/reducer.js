import { createAction, handleActions } from 'redux-actions'

/**
 * Constants
 */
export const SET_CONTROLLER_SETTING = 'SET_CONTROLLER_SETTING'

/**
 * Actions
 */
export const setControllerSetting = createAction(SET_CONTROLLER_SETTING)

/**
 * Action handlers
 */
const handleSetControllerSetting = (state, { payload }) => ({
  ...state,
  ...payload,
})

/**
 * Reducer
 */
export const initialState = {
  pressure: 30,
  speed: 20,
  volume: 600,
}

export const reducer = handleActions({
  [SET_CONTROLLER_SETTING]: handleSetControllerSetting,
}, initialState)

export default reducer
