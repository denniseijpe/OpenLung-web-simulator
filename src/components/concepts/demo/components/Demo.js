import React, { useState } from 'react'
import { Knob, Pointer, Scale } from 'rc-knob'
import { connect } from 'react-redux'

import { setControllerSetting } from '../../reducer'
import Pressure from '../../../vitals/Pressure'
import styles from './index.css'

// https://gitlab.com/open-source-ventilator/OpenLung/-/issues/67

/*

We should be able to set those settings from the knobs:

Speed (in BPM)
Volume (in liters)
PEEP pressure (in KPa)
PMAX pressure (in KPa)
sYnc (in % of cycle)
Inspiration ratio
Weight of the user (for air volume calculation)

Plus one button for On / Off action
For the alarms (probably leds + chimes):

High pressure
Low pressure
Powering outage/low battery
*/

const RANGES = {
  speed: {
    name: 'Speed',
    max: 40,
    min: 2,
    decimals: 0,
    steps: 19,
    default: 12,
    unit: 'BPM',
  },
  volume: {
    name: 'Volume',
    max: 3,
    min: 0.2,
    decimals: 2,
    steps: 28,
    default: 0.75,
    unit: 'L',
  },
  peep: {
    name: 'PEEP Pressure',
    max: 20,
    min: 0,
    decimals: 0,
    steps: 10,
    default: 4,
    unit: 'KPa',
  },
  pmax: {
    name: 'PMAX pressure',
    max: 10,
    min: 1,
    decimals: 0,
    steps: 9,
    default: 4,
    unit: 'KPa',
  },
  sync: {
    name: 'sYnc',
    max: 100,
    min: 0,
    decimals: 0,
    steps: 11,
    default: 50,
    unit: '%',
  },
  ratio: {
    name: 'Inspiration ratio',
    max: 1,
    min: 0,
    decimals: 1,
    steps: 10,
    default: 0.5,
    unit: '',
  },
  weight: {
    name: 'Weight of user',
    max: 200,
    min: 5,
    decimals: 0,
    steps: 10,
    default: 70,
    unit: 'kg',
  },
}

function defaultState() {
  const defs = {}
  Object.keys(RANGES).forEach((key) => {
    defs[key] = RANGES[key].default
  })
  return defs
}

// <text x="30" y="50">{`${value} ${specs.unit}`}</text>

function ScaledKnob({
  specs, slug, value, setValue,
}) {
  // angle range is 280, offset
  console.log(specs, value)
  return (
    <>
      <div className="knob-container">
        <span className="label">{specs.name}</span>
        <Knob
          size={100}
          angleOffset={220}
          angleRange={280}
          steps={specs.steps}
          min={specs.min}
          snap
          value={value}
          max={specs.max}
          onChange={(value) => {
            console.log(`Setting ${specs.name} = ${value}`)
            setValue(parseFloat(value.toFixed(specs.decimals)))
          }}
        >
          <Scale tickWidth={2} tickHeight={2} radius={45} />
          <circle r="35" cx="50" cy="50" fill="#FC5A96" />
          ,
          <Pointer
            width={2}
            height={35}
            radius={10}
            type="rect"
            color="#FC5A96"
          />
        </Knob>
      </div>
      <style jsx>{styles}</style>
    </>
  )
}

function Demo({ setControllerSetting, controllerSetting }) {

  return (
    <div className="App">
      <h1>Open Lung</h1>
      <div>
        Pressure:
        {' '}
        <Pressure />
      </div>
      <div className="mock-ui">
        {Object.keys(RANGES).map((key) => (
          <ScaledKnob
            key={key}
            specs={RANGES[key]}
            value={controllerSetting[key]}
            setValue={(value) => setControllerSetting({ [key]: value })}
          />
        ))}
      </div>
      <footer className="debug-readout">
        <div className="debug-boxes">
          {Object.keys(RANGES).map((key) => (
            <div key={key} className="debug-box">
              {`${RANGES[key].name}: ${controllerSetting[key]} ${RANGES[key].unit}`}
            </div>
          ))}
        </div>
      </footer>

      <style jsx>{styles}</style>
    </div>
  )
}

const mapStateToProps = (state) => ({
  controllerSetting: state.controller,
})

const mapDispatchToProps = ({
  setControllerSetting,
})

export default connect(mapStateToProps, mapDispatchToProps)(Demo)
