import React from 'react'
import { shallow } from 'enzyme'

import Drawer from '.'

describe('Drawer', () => {
  describe('[component] Drawer', () => {
    it('should show a link to P1A1', () => {
      const wrap = shallow(<Drawer />)

      expect(wrap.find('Link').filterWhere((a) => a.prop('href') === '/P1A1')).toHaveLength(1)
    })

    it('should show a link to Demo', () => {
      const wrap = shallow(<Drawer />)

      expect(wrap.find('Link').filterWhere((a) => a.prop('href') === '/demo')).toHaveLength(1)
    })
  })
})
