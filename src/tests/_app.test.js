import React from 'react'
import { shallow } from 'enzyme'

import App from '../pages/_app'

describe('Root', () => {
  describe('[component] App', () => {
    it('should add a Redux Provider', () => {
      const wrap = shallow(<App />).dive()
      const provider = wrap.find('Provider')

      expect(provider.prop('store')).toHaveProperty('dispatch')
    })
  })
})
