import breathingSimulatorReducer, {
  breathingSimulatorTick,
  BREATHING_SIMULATOR_TICK,
} from './breathing-simulator'

describe('BreathingSimulator', () => {
  describe('[module] breathing-simulator', () => {
    it('should export a reducer', () => {
      const state = breathingSimulatorReducer({ test: true }, {})

      // State should be the same
      expect(state).toEqual({ test: true })
    })

    it('should export constant BREATHING_SIMULATOR_TICK', () => {
      expect(BREATHING_SIMULATOR_TICK).toEqual('BREATHING_SIMULATOR_TICK')
    })

    it('should export action breathingSimulatorTick', () => {
      expect(breathingSimulatorTick()).toEqual({
        type: BREATHING_SIMULATOR_TICK,
      })
    })

    it('should handle BREATHING_SIMULATOR_TICK', () => {
      let state = breathingSimulatorReducer(undefined, {})

      // The pressureList should be empty
      expect(state.pressureList).toHaveLength(0)

      // Dispatch RUN_SIMULATOR
      state = breathingSimulatorReducer(state, {
        type: BREATHING_SIMULATOR_TICK,
      })

      // The pressureList should have an element
      expect(state.pressureList).toHaveLength(1)
    })
  })
})
