import { combineReducers, createStore } from 'redux'

import { reducer as breathingSimulatorReducer } from '../components/BreathingSimulator'
import controllerReducer from '../components/concepts/reducer'

// Combine the reducers
const rootReducer = combineReducers({
  breathingSimulator: breathingSimulatorReducer,
  controller: controllerReducer,
})

// eslint-disable-next-line no-undef, no-underscore-dangle
const devtools = typeof window !== 'undefined' && window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()

const makeStore = (initialState, options) => createStore(
  rootReducer,
  initialState,
  devtools || undefined,
)

export default makeStore
