import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { Grid, Paper, Typography } from '@material-ui/core'

import LCD from '../../widgets/LCD'
import P1A1Button from '../../widgets/P1A1Button'

/**
 * Apply padding to a number.
 * @param {Number} num - Number to format
 * @param {Number} padding - Amount of padding to apply
 */
const format = (num, padding) => (
  String(num).padStart(padding)
)

const P1A1 = ({ pressure, speed, volume }) => (
  <div className="container">
    <Typography variant="h3">
      P1A1
    </Typography>
    <Paper>
      <Grid container alignItems="center" direction="column">
        <div className="lcd">
          <LCD
            backlit
            line1={` S${format(speed, 2)} V${format(volume, 3)} P${format(pressure, 2)}`}
            line2="&nbsp;START VC"
            width={300}
          />
        </div>
        <Grid container justify="space-between" alignItems="center">
          <div>
            <P1A1Button color="blue" />
            <br />
            <P1A1Button color="blue" />
          </div>
          <div>
            <P1A1Button color="green" />
          </div>
          <div>
            <P1A1Button color="blue" />
            <br />
            <P1A1Button color="blue" />
          </div>
        </Grid>
      </Grid>
    </Paper>

    <style jsx>
      {`
        .container {
          margin-bottom: 2em;
        }

        .container > :global(.MuiPaper-root) {
          width: 50em;
          margin-left: auto;
          margin-right: auto;
          padding-top: 4em;
          padding-right: 6em;
          padding-left: 6em;
          padding-bottom: 3em;
        }

        .lcd {
          padding-bottom: 3em;
        }
      `}
    </style>
  </div>
)

P1A1.propTypes = {
  pressure: PropTypes.number,
  speed: PropTypes.number,
  volume: PropTypes.number,
}

P1A1.defaultProps = {
  pressure: 0,
  speed: 0,
  volume: 0,
}

const mapStateToProps = (state) => ({
  pressure: state.controller.pressure,
  speed: state.controller.speed,
  volume: state.controller.volume,
})

export default connect(mapStateToProps)(P1A1)
