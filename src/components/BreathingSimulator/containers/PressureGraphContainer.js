import { connect } from 'react-redux'

import PressureGraph from '../components/PressureGraph'

const mapStateToProps = (state) => ({
  pressureList: state.breathingSimulator.pressureList,
})

export default connect(mapStateToProps)(PressureGraph)
