import React from 'react'
import { shallow } from 'enzyme'
import PressureGraph from './PressureGraph'

describe('BreathingSimulator', () => {
  describe('[component] PressureGraph', () => {
    it('renders a VictoryChart', () => {
      const wrap = shallow(<PressureGraph />)
      expect(wrap.find('VictoryChart')).toHaveLength(1)
    })
  })
})
