import React from 'react'
import { shallow } from 'enzyme'

import BreathingMonitor from './BreathingMonitor'

describe('BreathingSimulator', () => {
  describe('[component] BreathingMonitor', () => {
    it('should show a PressureGraph', () => {
      const wrap = shallow(<BreathingMonitor />)

      expect(wrap.find('Connect(PressureGraph)')).toHaveLength(1)
    })

    it('should include a react interval component', () => {
      const wrap = shallow(<BreathingMonitor />)

      expect(wrap.find('ReactInterval')).toHaveLength(1)
    })
  })
})
