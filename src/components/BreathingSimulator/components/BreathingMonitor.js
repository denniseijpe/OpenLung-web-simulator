import React from 'react'
import PropTypes from 'prop-types'
import Card from '@material-ui/core/Card'
import CardContent from '@material-ui/core/CardContent'
import Typography from '@material-ui/core/Typography'
import ReactInterval from 'react-interval'
import css from 'styled-jsx/css'

import PressureGraph from '../containers/PressureGraphContainer'

const { className: drawerClassName, styles } = css.resolve`
  .MuiPaper-root {
    position: fixed;
    bottom: 1em;
    left: 1em;
    z-index: 1201;
  }
`

/**
 * Renders a graph for monitoring the simulated values.
 */
const BreathingMonitor = ({ breathingSimulatorTick, fps }) => (
  <Card className={drawerClassName}>
    <ReactInterval
      timeout={1000 / fps}
      enabled
      callback={breathingSimulatorTick}
    />
    <CardContent>
      <Typography color="textSecondary" gutterBottom>
        Breathing simulator
      </Typography>
      <PressureGraph />
    </CardContent>
    {styles}
  </Card>
)

BreathingMonitor.propTypes = {
  breathingSimulatorTick: PropTypes.func,
  fps: PropTypes.number,
}

BreathingMonitor.defaultProps = {
  breathingSimulatorTick: () => {},
  fps: 30,
}

export default BreathingMonitor
